<?php
require_once "Question.php";
session_start();
function init()
{
    $string = file_get_contents("./data/mock.json");
    $array = json_decode($string, true);

    $result = [];

    foreach ($array['questions'] as $item) {
        $result[] = new Question($item);
    }

    $_SESSION['questions'] = $result;
}

function getQuestions(int $page): array
{
    return array_chunk($_SESSION["questions"], 5)[$page];
}

function getCurrentQuestions(): array
{
    return getQuestions(getCurrentPage() - 1);
}

function getCurrentPage(): int
{
    if (!isset($_SESSION['page'])) {
        $_SESSION['page'] = 1;
    }

    return $_SESSION['page'];
}

function getResult() : int
{
    $questions = $_SESSION['questions'];
    $answers = $_SESSION['answers'];
    $map = [];

    foreach ($questions as $question) {
        $map['question-' . $question->id] = $question->correctAnswer;
    }

    $mark = 0;

    foreach ($answers as $key => $value) {
        if ($value === $map[$key]) $mark++;
    }

    unset($_SESSION['answers']);
    return $mark;
}