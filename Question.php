<?php

class Question
{
    public string $question ;
    public int $id;
    public array $answers;
    public string $correctAnswer;

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->question = $data['question'];
        $this->answers = $data['answers'];
        $this->correctAnswer = $data['correct_answer'];
    }
}