<?php
session_start();
function handleSubmit()
{
    $data = $_POST;
    foreach ($data as $key => $value) {
        $_SESSION['answers'][$key] = $value;
    }
    $_SESSION['page']++;
    if ($_SESSION['page'] === 3) {
        $_SESSION['page'] = 1;
        return header('location: result.php');
    }
    header("location: index.php");
}

handleSubmit();