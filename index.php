<?php
require_once "function.php";
session_start();
init();


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3"
            crossorigin="anonymous"></script>
</head>
<body>

<div class="container">

    <div class="d-flex justify-content-center">
        <form action="submit.php" method="post">
            <?php foreach (getCurrentQuestions() as $question): ?>
                <div class="question">
                    <div>
                        <h3><?= $question->question ?></h3>
                    </div>

                    <?php foreach ($question->answers as $answer): ?>
                        <div class="form-check">
                            <input name="<?='question-'.$question->id ?>" class="form-check-input" type="radio" value="<?=$answer["key"]?>"
                                   id="answer-<?= $question->id ?>-<?= $answer['key'] ?>">
                            <label class="form-check-label" for="answer-<?= $question->id ?>-<?= $answer['key'] ?>">
                                <?= $answer["value"] ?>
                            </label>
                        </div>
                    <?php endforeach ?>

                </div>
            <?php endforeach ?>

            <button disabled class="btn btn-success btn-submit">Next</button>
        </form>
    </div>
</div>


</body>

<script>
    function validateHasFullAnswer() {
        const questions = document.querySelectorAll(".question");
        return Array.from(questions).every(item => Boolean(item.querySelector("input[type='radio']:checked")))
    }

    const btnSubmit = document.querySelector(".btn-submit");

    document.querySelector("form").onsubmit = function(event) {
       if (!validateHasFullAnswer()) {
           event.preventDefault();
       }
    }

    document.querySelectorAll("input[type='radio']").forEach(item => {
        item.onchange = function() {
            if (validateHasFullAnswer()) {
                btnSubmit.removeAttribute("disabled");
            }
         }
    })
</script>
</html>

